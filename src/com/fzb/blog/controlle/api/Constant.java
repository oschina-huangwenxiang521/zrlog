package com.fzb.blog.controlle.api;

public interface Constant {

	public static final String CHANGEPWDSUCC="变更密码成功";
	
	public static final String OLDPWDERROR="旧密码错误";
	
	public static final String ARGSCHECKFAIL="检查输入参数是否正确";
}
