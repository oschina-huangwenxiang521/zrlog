﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</div>
</div>
<div style="clear:both;"></div>
<div id="footerbar">
  <footer>
    <div class="footer-right">
      <p><cite>&copy; 2014 ${webs.title}</cite></p>
      <p>Powered by <a href="http://zrlog.com" target="_blank">zrlog</a></p>
    </div>
    <div class="clearfix"></div>
  </footer>
</div>
<div style="display:none">${webs.webCm}</div>
</body>
</html>

